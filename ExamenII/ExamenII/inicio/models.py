from django.db import models

# Create your models here.

class Software(models.Model):
	nombre = models.CharField(max_length=32)
	funcion = models.CharField(max_length=32)
	departamento = models.CharField(max_length=32)

	def __str__(self):
		return self.nombre

class Departamento(models.Model):
	departamento = models.CharField(max_length=32)

	def __str__(self):
		return self.departamento