from django import forms

from .models import Software
from .models import Departamento

class SoftwareForm(forms.ModelForm):
	class Meta:
		model = Software
		fields = [
			"nombre",
			"funcion",
			"departamento",
		]

class DepartamentoForm(forms.ModelForm):
	class Meta:
		model = Software
		fields = [
			"departamento"
		]