from django.shortcuts import render

# Create your views here.

from .models import Software
from .models import Departamento
from .forms import SoftwareForm
from .forms import DepartamentoForm

def index(request):
	context = {

	}
	return render(request, "inicio/index.html", context)

def about(request):
	context = {

	}
	return render(request, "inicio/about.html", context)

#C R U D
#Create
def create(request):
	form = SoftwareForm(request.POST or None)
	if request.user.is_authenticated:
		error = "Acabas de iniciar sesion"
		if form.is_valid():
			form.save()
	else:
		error = "Debes de iniciar sesion"
	context = {
		"form": form,
		"Mensaje": error
	}
	return render(request, "inicio/crear.html", context)

#Retrieve
def detalles(request, id):
	queryset = Software.objects.get(id=id)
	context = {
		"object": queryset
	}
	return render(request, "inicio/detalles.html", context)

def lista(request):
	context = {
		"listaSoftware": Software.objects.all()
	}
	return render(request, "inicio/lista.html", context)

def departamento(request):
	form = DepartamentoForm(request.POST or None)
	if request.user.is_authenticated:
		error = "Acabas de iniciar sesion"
		if form.is_valid():
			form.save()
	else:
		error = "Debes de iniciar sesion"
	context = {
		"form": form,
		"Mensaje": error
	}
	return render(request, "inicio/registrarDepartamento.html", context)